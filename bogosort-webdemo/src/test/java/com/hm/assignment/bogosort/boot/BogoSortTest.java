package com.hm.assignment.bogosort.boot;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.hm.assignment.bogosort.BogoSort;
import com.hm.assignment.bogosort.BogoSortData;
import com.hm.assignment.bogosort.BogoSortUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BogoSortTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testSort() {
    	
    	BogoSort bogoSort = new BogoSort(BogoSortUtil.asList2("7, 4, 5, 9, 3"));
    	Assert.assertTrue(bogoSort.getSortedElements().toString().equals("[3, 4, 5, 7, 9]"));
    }
    
    @Test
    public void testIndex() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("BogoSort-Demo")));
    }
    
    @Test
    public void testAdd() throws Exception {
    	
    	Integer [] i = { 32, 57, 42, 19, 50, 27, 42, 26, 76, 47, 66 };
    	ArrayList<Integer> elements = new ArrayList<>();
		
		CollectionUtils.addAll(elements, i);
    	
        mvc.perform(MockMvcRequestBuilders.post("/addSort")
        			.content(elements.toString())
        			.contentType(MediaType.APPLICATION_JSON)
        			.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Done")));
    }

    @Test
    public void testGetAll() throws Exception {
    	
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/all")
    			.accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
		
		String jsonContent = mvcResult.getResponse().getContentAsString();
		BogoSortData data = BogoSortUtil.asData(jsonContent);
		
		Assert.assertTrue(data.getAllSorts().size() == 1);
		
		List<BogoSort> allSorts = data.getAllSorts();
		
		BogoSort bogoSort = allSorts.get(0);
		
		Assert.assertEquals(bogoSort.getSortId(), 1);
		Assert.assertTrue(bogoSort.getTotalShuffles() > 0);
		
		System.out.println(bogoSort);

    }
    
}
