package com.hm.assignment.bogosort.boot;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hm.assignment.bogosort.BogoSortService;
import com.hm.assignment.bogosort.BogoSortServiceException;
import com.hm.assignment.bogosort.BogoSortUtil;

@Controller
public class BogoSortControllerWeb {

	@Autowired
	BogoSortService bogoSortService;

	@RequestMapping(path = "/home")
	public String home(Map<String, Object> model) {

		model.put("elements", bogoSortService.getAllSortData());

		return "home";
	}

	@RequestMapping(path = "/addSort2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addSort2(@RequestParam("elements") String elements, Map<String, Object> model) {

		try {
			
			bogoSortService.addSortData(BogoSortUtil.asList2(elements));
			model.put("elements", bogoSortService.getAllSortData());
			
		} catch (BogoSortServiceException e) {
			e.printStackTrace();
			return "Error";
		}

		return "home";
	}

}
