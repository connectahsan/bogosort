package com.hm.assignment.bogosort;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BogoSortUtil {


    public static List<Integer> asList(String jsonElements) {
    	
        try {
            final ObjectMapper mapper = new ObjectMapper();
            @SuppressWarnings("unchecked")
			final List<Integer> data = mapper.readValue(jsonElements, List.class);
            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    	
    }

    public static List<Integer> asList2(String elements) {
    	
        try {

        	List<Integer> elementsList = new ArrayList<>();
        	String[] strings = elements.split(",");
        	
        	for (String string : strings) {
				
        		elementsList.add(Integer.parseInt(string.trim()));
			}
        	
            return elementsList;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    	
    }
	
    public static BogoSortData asData(String listOfSorts) {
    	
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final BogoSortData data = mapper.readValue(listOfSorts, BogoSortData.class);
            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    	
    	
    }
    
    public static String asJsonString(final BogoSortData obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    

}
