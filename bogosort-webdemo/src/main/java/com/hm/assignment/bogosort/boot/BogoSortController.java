package com.hm.assignment.bogosort.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.assignment.bogosort.BogoSortData;
import com.hm.assignment.bogosort.BogoSortService;
import com.hm.assignment.bogosort.BogoSortServiceException;
import com.hm.assignment.bogosort.BogoSortUtil;

@RestController
public class BogoSortController {

	@Autowired
	BogoSortService bogoSortService;
	
	@RequestMapping(path = "/")
	public String index() {
		return "BogoSort-Demo";
	}
	
	@RequestMapping(value = "/addSort", method = RequestMethod.POST)
	@ResponseBody
	public String addSort(@RequestBody String data) {
		
		try {
			bogoSortService.addSortData(BogoSortUtil.asList(data));
		} catch (BogoSortServiceException e) {
			e.printStackTrace();
			return "Error";
		}
		
		return "Done";
	}
	
	@RequestMapping("/all")
	public BogoSortData getAllSorts() {
		return new BogoSortData(bogoSortService.getAllSortData());
	}
	
	@RequestMapping("/test")
	public String getHome() {
		
		return "html/home";
	}
	
	
}
