package com.hm.assignment.bogosort.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.assignment.bogosort.BogoSort;
import com.hm.assignment.bogosort.BogoSortRepository;
import com.hm.assignment.bogosort.BogoSortRepositoryException;
import com.hm.assignment.bogosort.BogoSortService;
import com.hm.assignment.bogosort.BogoSortServiceException;

/**
 * Service implementation of {@link BogoSortService}
 * 
 * @author ahsan.saleem
 *
 */
@Service
public class BogoSortServiceImpl implements BogoSortService {

	@Autowired
	private BogoSortRepository bogoSortRepository;
	
	@Override
	public List<BogoSort> getAllSortData() {
		return bogoSortRepository.getAll();

	}

	@Override
	public boolean addSortData(List<Integer> elements) throws BogoSortServiceException {

		try {
			bogoSortRepository.add(new BogoSort(elements));
			return true;
		} catch (BogoSortRepositoryException e) {
			throw new BogoSortServiceException(e.getMessage());
		}

	}

	@Override
	public boolean removeAllSortData() throws BogoSortServiceException {
		
		try {
			bogoSortRepository.removeAll();
		} catch (BogoSortRepositoryException e) {
			throw new BogoSortServiceException(e.getMessage());
		}
		return false;
	}

}
