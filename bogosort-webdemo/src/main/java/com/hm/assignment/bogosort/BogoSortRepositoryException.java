package com.hm.assignment.bogosort;

/**
 * Exception wrapper for {@link BogoSortRepository}
 * 
 * @author ahsan.saleem
 *
 */
public class BogoSortRepositoryException extends Exception {


	private static final long serialVersionUID = 1L;

	public BogoSortRepositoryException() {
		
	}

	public BogoSortRepositoryException(String message) {
		super(message);
	}

	public BogoSortRepositoryException(Throwable cause) {
		super(cause);
	}

}
