package com.hm.assignment.bogosort;

import java.util.List;

public class BogoSortData {

	private List<BogoSort> allSorts;

	public BogoSortData() {
		
	}

	public BogoSortData(List<BogoSort> allSorts) {
		this.allSorts = allSorts;
	}

	public void setAllSorts(List<BogoSort> allSorts) {
		this.allSorts = allSorts;
	}

	public List<BogoSort> getAllSorts() {
		return allSorts;
	}
	
}
