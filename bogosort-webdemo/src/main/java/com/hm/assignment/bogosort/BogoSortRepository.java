package com.hm.assignment.bogosort;

import java.util.List;

/**
 * The repository for the BogoSort entity.
 * 
 * @author ahsan.saleem
 *
 */
public interface BogoSortRepository {

	public void add(BogoSort data) throws BogoSortRepositoryException;
	
	public void removeAll() throws BogoSortRepositoryException;
	
	public List<BogoSort> getAll();
	
}
