package com.hm.assignment.bogosort.boot;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan({ "com.hm.assignment.bogosort.impl", "com.hm.assignment.bogosort.boot" })
@EntityScan({ "com.hm.assignment.bogosort" })
public class BogoSortSpringBootApplication extends SpringBootServletInitializer {

	private static final Log logger = LogFactory.getLog(BogoSortSpringBootApplication.class);

	public static void main(String[] args) {

		ApplicationContext ctx = SpringApplication.run(BogoSortSpringBootApplication.class, args);

		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			logger.debug(beanName);
		}
	}
}