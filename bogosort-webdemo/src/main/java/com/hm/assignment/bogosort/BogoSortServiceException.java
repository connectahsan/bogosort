package com.hm.assignment.bogosort;


/**
 * Exception wrapper for {@link BogoSortService}
 * 
 * @author ahsan.saleem
 *
 */
public class BogoSortServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BogoSortServiceException() {
		
	}

	public BogoSortServiceException(String arg0) {
		super(arg0);
	}

	public BogoSortServiceException(Throwable arg0) {
		super(arg0);
	}

}
