package com.hm.assignment.bogosort;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "BogoSort")
public class BogoSort {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long sortId;
	
	@Transient
	private List<Integer> elements;
	
	@Transient
	private List<Integer> sortedElements;

	@Column(name = "elements")
	private String elements_;
	
	@Column(name = "sorted_elements")
	private String sortedElements_;
	
	@Column
	private long totalShuffles;

	@Column
	private long duration;

	public BogoSort() {
		
	}
	
	public BogoSort(List<Integer> elements) {

		setElements(elements);
		setSortedElements(new ArrayList<Integer>(elements));
		
		long startTime = System.currentTimeMillis();
		
		long counter = 0;
		while (!isSorted()) {
			randomize();
			counter++;
		}
		
		long endtime = System.currentTimeMillis();
		
		this.totalShuffles = counter;
		this.duration = endtime - startTime;
		
		this.elements_ = elements.toString();
		this.sortedElements_ = sortedElements.toString();

	}

	private void randomize() {
		
		for (int x = 0; x < sortedElements.size(); ++x) {
			
			int index1 = (int) (Math.random() * sortedElements.size());
			int index2 = (int) (Math.random() * sortedElements.size());
			
			int a = sortedElements.get(index1);
			
			sortedElements.set(index1, sortedElements.get(index2));
			sortedElements.set(index2, a);
		}
	}

	private boolean isSorted() {
		for (int x = 0; x < sortedElements.size() - 1; x++) {
			if (sortedElements.get(x) > sortedElements.get(x + 1)) {
				return false;
			}
		}
		return true;
	}

	public long getSortId() {
		return sortId;
	}

	public void setSortId(long sortId) {
		this.sortId = sortId;
	}

	public List<Integer> getElements() {
		if(elements == null) {
			elements = BogoSortUtil.asList(elements_);
		}
		return elements;
	}

	public void setElements(List<Integer> elements) {
		this.elements = elements;
		this.elements_ = elements.toString();
	}

	public List<Integer> getSortedElements() {
		if(sortedElements == null) {
			sortedElements = BogoSortUtil.asList(sortedElements_);
		}
		return sortedElements;
	}

	public void setSortedElements(List<Integer> sortedElements) {
		this.sortedElements = sortedElements;
		this.sortedElements_ = sortedElements.toString();
	}

	public long getTotalShuffles() {
		return totalShuffles;
	}

	public void setTotalShuffles(long totalShuffles) {
		this.totalShuffles = totalShuffles;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "BogoSort [sortId : " + sortId + ", elements : " + elements + ", sortedElements : " + sortedElements + ", totalShuffles : " + totalShuffles + ", duration : " + duration + "]";
	}
	
	
}
