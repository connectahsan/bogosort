package com.hm.assignment.bogosort;

import java.util.List;

/**
 * The main contract for the application which provides basic functionality around the sort data result.
 * 
 * @author ahsan.saleem
 *
 */
public interface BogoSortService {

	List<BogoSort> getAllSortData();
	
	boolean addSortData(List<Integer> elements) throws BogoSortServiceException;
	
	boolean removeAllSortData() throws BogoSortServiceException;
	
}
