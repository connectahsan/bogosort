package com.hm.assignment.bogosort.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hm.assignment.bogosort.BogoSort;
import com.hm.assignment.bogosort.BogoSortRepository;
import com.hm.assignment.bogosort.BogoSortRepositoryException;

/**
 * Repository Implementation for the BogoSortRepository
 * 
 * @author ahsan.saleem
 *
 */
@Transactional
@Repository
public class BogoSortRepositoryImpl implements BogoSortRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void add(BogoSort data) throws BogoSortRepositoryException {
		
		Validate.notNull(data);
		
		try {
		
			entityManager.persist(data);
			entityManager.flush();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new BogoSortRepositoryException(e);
		}
	}

	@Override
	public void removeAll() {
		
		//	TODO: Doesn't need to be implemented for now.
	}

	@Override
	public List<BogoSort> getAll() {

		String sql = "select b from BogoSort b ORDER BY b.sortId";
		List<BogoSort> result = (List<BogoSort>) entityManager.createQuery(sql).getResultList();
		return result;
	}
	
	

}
