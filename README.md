# README #

This is a sample to show the microservices through the Spring Boot.

### What is this repository for? ###

* Bogosort is the Spring Boot based full stack web application. It uses mainly JPA, Hibernate, Thymeleaf & Bootstrap as underlying components.
* Current Version : 1.0-SNAPSHOT

### How do I get set up? ###

* Install Java 8 SDK & Maven 3 (e.g. Initial setup would look like this) :
> mvn -version

Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T17:41:47+01:00)

Maven home: C:\Tools\apache-maven-3.3.9\bin\..

Java version: 1.8.0_121, vendor: Oracle Corporation

Java home: C:\Program Files\Java\jdk1.8.0_121\jre

Default locale: en_US, platform encoding: Cp1252

OS name: "windows 10", version: "10.0", arch: "amd64", family: "dos"

* Build : 

> mvn clean install (will build and run junit test with inMemory H2 database)

* Run/Deploy :

> java -jar target\bogosort-webdemo-1.0-SNAPSHOT.jar (will run the web application on port 8080 along with file based H2 database located in target folder)

* Web Demo :

> Open link : localhost:8080/home

Enter a list of numbers by comma separated values, by clicking on "Sort" button, the elements will be sent to the server and sort result will be persisted and home page will present it in tabular structure.

You can optionally send sort data using curl : 

> curl -H "Content-Type: application/json" -X POST -d "[32,57,42,19,50,27,42,26,76,47,66]" "localhost:8080/addSort"

To see sort data on command line : 

> curl localhost:8080/all

### Contact ###

* [Ahsan Saleem](mailto:ahsan@kth.se)